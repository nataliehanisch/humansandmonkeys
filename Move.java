/**
 * Natalie Hanisch
 * Spring 2015
 * CSIT441: Artificial Intelligence
 */

package humansAndMonkeys;

/**
 * This class has moves which can be used to modify the state of the game.
 * @author natalie
 *
 */
public class Move {
	
	private int[] hma; //hma = humans monkeys, apes
	
	/**
	 * This method constructs a move. A move contains only three numbers,
	 * The number of humans, monkeys, and apes that will move from
	 * one side to the other.
	 * @param humans
	 * @param monkeys
	 * @param apes
	 */
	public Move(int humans, int monkeys, int apes) {
		this.hma = new int[] {humans, monkeys, apes};
	}
	
	/**
	 * This is the toString method, which simply prints all three numbers.
	 */
	public String toString() {
		return getHumans() + "\t" + this.getMonkeys() + "\t" + this.getApes();
	}
	
	/**
	 * This returns the values associated with the move.
	 * @return
	 */
	public int[] getMove() {
		return this.hma;
	}
	
	/**
	 * This returns the number of humans associated with the move.
	 * @return
	 */
	public int getHumans() {
		return hma[0];
	}
	
	/**
	 * This returns the number of monkeys associated with the move.
	 * @return
	 */
	public int getMonkeys() {
		return hma[1];
	}
	
	/**
	 * This returns the number of apes associated with the move.
	 * @return
	 */
	public int getApes() {
		return hma[2];
	}
	
}
