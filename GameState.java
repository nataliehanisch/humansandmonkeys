/**
 * Natalie Hanisch
 * Spring 2015
 * CSIT441: Artificial Intelligence
 */

package humansAndMonkeys;

import java.util.Arrays;

/**
 * This class is the current state for the humans
 * and monkeys puzzle. It has five basic attributes:
 * Number of humans on side 0 and side 1,
 * Number of monkeys on side 0 and side 1,
 * Number of apes on side 0 and side 1, and
 * which side the boat is on.
 * 
 * @author natalie
 *
 */
public class GameState {
	
	protected int numHumans0;
	protected int numHumans1;
	protected int numApes0;
	protected int numApes1;
	protected int numMonkeys0;
	protected int numMonkeys1;
	protected int boatState;
	protected int boatCapacity;
	protected int[] prevState;
	protected Boolean gsValid = true;
	
	/**
	 * Constructs the gamestate based on an initial number
	 * of humans. All other basic values are determined
	 * from this value.
	 * @param numberOfHumans
	 */
	public GameState(int numberOfHumans) {
		
		this.numHumans0 = numberOfHumans;
		this.numHumans1 = 0;
		this.numApes0 = 1;
		this.numApes1 = 0;
		this.numMonkeys0 = numberOfHumans - 1;
		this.numMonkeys1 = 0;
		this.boatState = 0;
		this.boatCapacity = determineBoatCapacity(numberOfHumans);
		prevState = this.getCurrentState();
	}
	
	/**
	 * Returns the current state of the game as a string of
	 * comma delimited integers. The order is :
	 * "[humans side 0, monkeys side 0, apes side 0,
	 * humans side 1, monkeys side 1, apes side 1,
	 * boat side]"
	 */
	public String toString() {
		return Arrays.toString(getCurrentState());
	}
	
	/**
	 * Takes a move and updates the current state of the game.
	 * @param move
	 */
	public void updateGameState(Move move) {

		if (boatState == 0) {
			this.numHumans0 -= move.getHumans();
			this.numHumans1 += move.getHumans();
			this.numApes0 -= move.getApes();
			this.numApes1 += move.getApes();
			this.numMonkeys0 -= move.getMonkeys();
			this.numMonkeys1 += move.getMonkeys();
			this.boatState = 1;
		}

		else { //boatState == 1
			this.numHumans0 += move.getHumans();
			this.numHumans1 -= move.getHumans();
			this.numApes0 += move.getApes();
			this.numApes1 -= move.getApes();
			this.numMonkeys0 += move.getMonkeys();
			this.numMonkeys1 -= move.getMonkeys();
			this.boatState = 0;
		}
		
		if (!checkStateValidity()) {
			gsValid = false;
		}

	}
	
	/**
	 * Checks current state of game for validity according
	 * to the rule that the number of monkeys and apes on
	 * either side of the river must not exceed the number
	 * of humans on that same side of the river (unless
	 * there are no humans on that side).
	 * @return true if valid, false otherwise
	 */
	public Boolean checkStateValidity() {
		
		if (numHumans0 > 0 && (numHumans0 < numApes0 + numMonkeys0) || 
				numHumans1 > 0 && (numHumans1 < numApes1 + numMonkeys1)) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Returns current state of the game in an int[] array.
	 * @return
	 */
	public int[] getCurrentState() {
		int[] currentState =  {	this.numHumans0, this.numMonkeys0, this.numApes0,
								this.numHumans1, this.numMonkeys1, this.numApes1, 
								this.boatState};
		return currentState;
	}
	
	/**
	 * Determines capacity of the boat based on the initial
	 * number of humans chosen. 
	 * For n < 3, capacity = 2,
	 * for n = 4 or n = 5, capacity = 3
	 * for n > 5, capacity = 4
	 * 
	 * @param numOfHumans
	 * @return
	 */
	public static int determineBoatCapacity(int numOfHumans) {
		
		if (numOfHumans > 0) {

			switch (numOfHumans) {
			case 1:
			case 2:
			case 3:
				return 2;
			case 4:
			case 5:
				return 3;
			default:
				return 4;
			}
		}
		else
			return 0;
		
	}

	/**
	 * Returns true if game is at solution state and false otherwise.
	 * @return
	 */
	public boolean solved() {
		if (this.numHumans0 + this.numMonkeys0 + this.numApes0 == 0)
			return true;
		else
			return false;
	}

	/**
	 * Overrides the hashcode method to give each GameState
	 * a unique hashcode which is equivalent to their
	 * current state. The hashcode is an integer where
	 * each power of ten place-value holds a particular
	 * number associcated with it's state.
	 */
	public int hashCode() {
		
		int code = (int) (this.numHumans0*Math.pow(10, 6)
				+ this.numMonkeys0*Math.pow(10, 5) 
				+ this.numApes0*Math.pow(10, 4)
				+ this.numHumans1*Math.pow(10, 3) 
				+ this.numMonkeys1*Math.pow(10, 2) 
				+ this.numApes1*10
				+ this.boatState);
		
		return code;
		
	}

	
	/**
	 * Given a state, reverts the gamestate to that state.
	 * @param state
	 */
	public void revert(int[] state) {
		prevState = state;
		
		this.numHumans0 = state[0];
		this.numHumans1 = state[3];
		this.numApes0 = state[2];
		this.numApes1 = state[5];
		this.numMonkeys0 = state[1];
		this.numMonkeys1 = state[4];
		this.boatState = state[6];
	}
	
	/**
	 * Returns the "value" of a given state. This is a heuristic
	 * based on the idea that higher value states are those which have 
	 * the most hominids on side 1 of the river. Thus the heuristic
	 * is the sum of party members on side 1.
	 * @return
	 */
	public int getValue() {
		return this.numHumans1 + this.numMonkeys1 + this.numApes1; 
	}
	
}
