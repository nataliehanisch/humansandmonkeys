/**
 * Natalie Hanisch
 * Spring 2015
 * CSIT441: Artificial Intelligence
 */

package humansAndMonkeys;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * This class implements the Depth-First Search algorithm.
 * @author natalie
 *
 */
public class DepthFirstSearch {

	static Hashtable<GameState, GameState> prevStates
	= new Hashtable<GameState, GameState>();
	static ArrayList<int[]> solutionPath = new ArrayList<int[]>();

	/**
	 * Given a number of humans, this initializes the algorithm
	 * and returns a 2D array containing the series of GameStates
	 * needed to reach the solution found.
	 * @param n
	 * @return
	 */
	public static int[][] initializeDFSalgorithm(int n) {

		GameState gs = new GameState(n);
		Boolean solved = false;
		solutionPath.add(gs.getCurrentState());

		while (!solved) {
			solved = findSolution(gs);}		

		int[][] solutionArray = solutionPath.toArray(new int[solutionPath.size()][]);
		return solutionArray;

	}

	/**
	 * The basic DFS algorithm for finding a solution.
	 * Fully expands each branch before moving on the the next.
	 * The DFS is implemented via recursion.
	 * @param gs
	 * @return
	 */
	private static Boolean findSolution(GameState gs) {

		Move[] moves = generatePossibleMoves(gs);
		Boolean solved = false;
		prevStates.put(gs, gs);
		int[] origState;

		int index = 0;
		while(!solved && index < moves.length) {

			if (solved || gs.solved())
				return true;

			origState = gs.getCurrentState();
			gs.updateGameState(moves[index]);

			if (gs.checkStateValidity() && !checkIfVisited(gs)) {

				solutionPath.add(gs.getCurrentState());
				solved = findSolution(gs);

				if (solved || gs.solved())
					return true;
			}
			gs.revert(origState);
			index++;

		}
		solutionPath.remove(solutionPath.size() - 1);

		return false;
	}

	/**
	 * Checks against a list of visited states to see if
	 * the given GameState has been visited yet.
	 * @param gs
	 * @return
	 */
	public static boolean checkIfVisited(GameState gs) {

		if(prevStates.get(gs) != null)
			return true;
		else
			return false;

	}

	/**
	 * Returns a list of possible moves for any given state.
	 * Some of the moves may result in an invalid state,
	 * but this is checked elsewhere.
	 * @param gs
	 * @return
	 */
	public static Move[] generatePossibleMoves(GameState gs) {

		int humans;
		int monkeys;
		int apes;
		Move[] moves;

		if (gs.boatState == 0) {
			humans = gs.numHumans0;
			monkeys = gs.numMonkeys0;
			apes = gs.numApes0;
			moves = createMoveList(humans, monkeys, apes, gs.boatCapacity);
		}
		else { //gs.boatState == 1
			humans = gs.numHumans1;
			monkeys = gs.numMonkeys1;
			apes = gs.numApes1;
			moves = createMoveList(humans, monkeys, apes, gs.boatCapacity);
		}

		return moves;
	}

	/**
	 * Generates a list of moves based on some basic rules:
	 * a) at least one human or ape
	 * b) no more than the maximum boat capacity can move
	 * And also uses only the hominids available at the given
	 * side of the river.
	 * 
	 * @param humans
	 * @param monkeys
	 * @param apes
	 * @param capacity
	 * @return
	 */
	private static Move[] createMoveList(int humans, int monkeys, int apes, int capacity) {

		ArrayList<Move> moves = new ArrayList<Move>();

		for (int h = humans; h >= 0; h--) {
			for (int m = monkeys; m >= 0; m--) {
				for (int a = apes; a >= 0; a--) {
					if (h+m+a <= capacity && h+m+a > 0 && h+a > 0) {
						Move move = new Move(h,m,a);
						moves.add(move);
					}
				}
			}
		}

		Move[] finalMoves = moves.toArray(new Move[moves.size()]);

		return finalMoves;
	}

}
