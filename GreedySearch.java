/**
 * Natalie Hanisch
 * Spring 2015
 * CSIT441: Artificial Intelligence
 */

package humansAndMonkeys;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class implements the Greedy Search Algorithm. 
 * @author natalie
 *
 */
public class GreedySearch {
	
	/**
	 * This method returns a 2D array containing (on each row)
	 * the states which, in consecuitive order give a correct
	 * solution. With this, the series of moves needed to reach
	 * that solution can be easily determined.
	 * @param n, number of humans to start with
	 * @return
	 */
	public static int[][] findSolution(int n) {
		
		GameState gs = new GameState(n);
		Boolean solved = false;
		ArrayList<int[]> solutionPath = new ArrayList<int[]>();
		solutionPath.add(gs.getCurrentState());
		
		//While the the solution is not found, continue trying
		// the best known move.
		while (!solved) {
			Move[] moves = DepthFirstSearch.generatePossibleMoves(gs);
			int[] values = calculateValues(moves, gs);
			int maxIndex = getMaxIndex(values);
			
			gs.updateGameState(moves[maxIndex]);
			solutionPath.add(gs.getCurrentState());
			
			if (gs.solved())
				solved = true;
		}
		
		int[][] solutionArray = solutionPath.toArray(new int[solutionPath.size()][]);
		return solutionArray;
	}

	/**
	 * Creates an array containing the "value" of each possible move.
	 * This is useful for deciding what the best move is.
	 * @param moves
	 * @param gs
	 * @return
	 */
	private static int[] calculateValues(Move[] moves, GameState gs) {
		
		int[] values = new int[moves.length];
		int[] initGS = gs.getCurrentState();
		
		for (int i = 0; i < values.length; i++) {
			gs.updateGameState(moves[i]);
			values[i] = gs.getValue();
			gs.revert(initGS);
		}
		
		return values;
	}

	/**
	 * Given an integer array, determines the largest value in the array.
	 * If there are duplicates, it will always choose the first large number.
	 * @param nums
	 * @return
	 */
	private static int getMaxIndex(int[] nums) {
		
		int max = nums[0];
		int maxIndex = 0;
		
		for (int i = 1; i < nums.length; i++) {
			if (nums[i] > max) {
				max = nums[i];
				maxIndex = i;
			}
		}
		return maxIndex;
	}

}
