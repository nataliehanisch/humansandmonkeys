/**
 * Natalie Hanisch
 * Spring 2015
 * CSIT441: Artificial Intelligence
 */

package humansAndMonkeys;

/**
 * This class implements two algorithms, Depth-First Search
 * and Greedy Search, and prints the solution found.
 * @author natalie
 *
 */
public class AlgorithmRunner {

	/**
	 * The main method runs the algorithms and notes how long
	 * they take to run.
	 * @param args
	 */
	public static void main(String[] args) {

		int n = 3;

		long startTime = System.currentTimeMillis();

		System.out.print("\n\nDFS Algorithm");
		System.out.print(getSolutionString(DepthFirstSearch.initializeDFSalgorithm(n)));

		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println("\nSolution Time:\t" + totalTime + " ms");

		startTime = System.currentTimeMillis();

		System.out.print("\n\nGreedy Algorithm");
		System.out.print(getSolutionString(GreedySearch.findSolution(n)));

		endTime = System.currentTimeMillis();
		totalTime = endTime - startTime;
		System.out.println("\nSolution Time:\t" + totalTime + " ms");
	}

	/**
	 * Returns a string containing the formatted solution
	 * found by an algorithm.
	 * @param sp
	 */
	private static String getSolutionString(int[][] sp) {

		String s = "";
		
		for (int i = 0; i < sp.length - 1; i++) {
			s += formatMove(sp[i], sp[i+1], i+1);			
		}

		s += "Solution depth:\t" + sp.length;
		
		return s;
	}

	/**
	 * Given two states, formats a string to represent the change 
	 * which has occurred between them.
	 * @param s1
	 * @param s2
	 * @return
	 */
	private static String formatMove(int[] s1, int[] s2, int moveNum) {

		String s = "\n\nH\tM\tA\t\t\tH\tM\tA" +  
				"\n\n" + s1[0] + "\t" + s1[1] + "\t" + s1[2] +
				"\t\t\t" + 
				s2[0] + "\t" + s2[1] + "\t" + s2[2] +
				"\n\t\t\t" + getArrow(s1[6]) +
				getChange(s1, s2) + "\t\t\t\t\t Move# " + moveNum +	"\n" + 
				s1[3] + "\t" + s1[4] + "\t" + s1[5] +
				"\t\t\t" +
				s2[3] + "\t" + s2[4] + "\t" + s2[5] + "\n\n";

		return s;
	}

	/**
	 * Determines and creates a string containing the mathematical 
	 * difference between humans, monkeys, and apes on either side
	 * of the river. 
	 * @param s1
	 * @param s2
	 * @return
	 */
	private static String getChange(int[] s1, int[] s2) {

		String s = "(" + Math.abs(s1[0] - s2[0]) + "," 
				+ Math.abs(s1[1] - s2[1]) + "," 
				+Math.abs(s1[2] - s2[2])+ ")";
		return s;
	}

	/**
	 * Determines the direction of an arrow needed to represent
	 * the given change between two states.
	 * @param i
	 * @return
	 */
	private static String getArrow(int i) {

		if (i == 0) //down arrow
			return "\u2193";
		else //up arrow
			return "\u2191";
	}

}
